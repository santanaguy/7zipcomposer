﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Projeto.Models;

namespace Projeto
{
    public partial class ZipOperationView : UserControl, IZipOperationView
    {
        private IZipOperationModel model;
        private ZipOperationController controller;

        public string Path => saveFileDialog1.FileName;
        public string File => openFileDialog1.FileName;
        public string Directory => folderBrowserDialog1.SelectedPath;
        public ItemType Type => rdbFile.Checked ? ItemType.File : ItemType.Directory;
        public string Password => txtPassword.Text;
        public string ConfirmPassword => txtConfirmPassword.Text;
        public string ArchiveFormat => cmbArchiveFormat.SelectedItem?.ToString();
        public string CompressionLevel => cmbCompressionLevel.SelectedItem?.ToString();
        public bool IncludeEmptyDirectories => chkIncludeEmpty.Checked;
        public event EventHandler OnSave;
        public event EventHandler<string> OnPathChanged;
        string logFilename = System.IO.Path.Combine(Application.ExecutablePath, "log.txt");

        public ZipOperationView()
        {
            InitializeComponent();
            model = new ZipOperationModel();
            model.OnSaveOperationFailed += Model_SaveOperationHappened;
            controller = new ZipOperationController(this, model, new Models.HistoryModel(logFilename));
        }

        private void Model_SaveOperationHappened(object sender, Result e)
        {
            MessageBox.Show(e.Message, e.Code, MessageBoxButtons.OK);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            var dialogResult = saveFileDialog1.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                txtName.Text = saveFileDialog1.FileName;
                OnPathChanged?.Invoke(this, new FileInfo(saveFileDialog1.FileName).Name);
            }

        }

        private void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                txtPath.Text = openFileDialog1.FileName;
            else
                txtPath.Clear();
        }

        private void RadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                txtPath.Text = folderBrowserDialog1.SelectedPath;
            else
                txtPath.Clear();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            OnSave?.Invoke(this, new EventArgs());
        }
    }
}
