﻿using Projeto.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projeto.Views
{
    public partial class SettingsForm : Form, ISettingsView
    {
        public string SevenZipPath => textBox1.Text;
        public event EventHandler OnSettingsChanged;

        public SettingsForm()
        {
            InitializeComponent();
            Models.SettingsModel model = new Models.SettingsModel();
            model.OnLoadFailed += Model_OnLoadFailed;
            SettingsController controller = new SettingsController(this, model);
        }

        private void Model_OnLoadFailed(object sender, Result e)
        {
            MessageBox.Show(e.Message, e.Code, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = openFileDialog1.FileName;
                OnSettingsChanged(this, new EventArgs());
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
