﻿using Projeto.Controllers;
using Projeto.Models;
using Projeto.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projeto
{
    public partial class Form1 : Form, IMainView
    {
        MainController controller;
        public Form1()
        {
            InitializeComponent();
            controller = new MainController(this);
        }

        public event EventHandler OnOpenSettings;

        private void Button1_Click(object sender, EventArgs e)
        {
            var tabPage = new TabPage("Ficheiro " + tabControl1.TabCount + 1);
            tabControl1.TabPages.Add(tabPage);

            ZipOperationView control = new ZipOperationView() { Dock = DockStyle.Fill };
            control.OnPathChanged += Control_OnPathChanged;
            tabPage.Controls.Add(control);
        }

        private void Control_OnPathChanged(object sender, string e)
        {
            tabControl1.Name = e;
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.OnOpenSettings?.Invoke(this, new EventArgs());
        }

        public void ShowSettingsView()
        {
            SettingsForm settings = new SettingsForm();
            settings.ShowDialog();
        }
    }
}
