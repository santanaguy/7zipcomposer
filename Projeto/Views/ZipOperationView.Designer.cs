﻿namespace Projeto
{
    partial class ZipOperationView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtConfirmPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbArchiveFormat = new System.Windows.Forms.ComboBox();
            this.cmbCompressionLevel = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkIncludeEmpty = new System.Windows.Forms.CheckBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.button2 = new System.Windows.Forms.Button();
            this.rdbFile = new System.Windows.Forms.RadioButton();
            this.rdbFolder = new System.Windows.Forms.RadioButton();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-1, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Password";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(84, 117);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(157, 20);
            this.txtPassword.TabIndex = 5;
            // 
            // txtName
            // 
            this.txtName.Enabled = false;
            this.txtName.Location = new System.Drawing.Point(84, 85);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(431, 20);
            this.txtName.TabIndex = 8;
            // 
            // txtConfirmPassword
            // 
            this.txtConfirmPassword.Location = new System.Drawing.Point(352, 117);
            this.txtConfirmPassword.Name = "txtConfirmPassword";
            this.txtConfirmPassword.PasswordChar = '*';
            this.txtConfirmPassword.Size = new System.Drawing.Size(163, 20);
            this.txtConfirmPassword.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(247, 124);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Confirmar password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(-1, 157);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Formato do arquivo";
            // 
            // cmbArchiveFormat
            // 
            this.cmbArchiveFormat.FormattingEnabled = true;
            this.cmbArchiveFormat.Items.AddRange(new object[] {
            "SevenZip",
            "Zip",
            "GZip",
            "BZip2",
            "Tar",
            "XZ"});
            this.cmbArchiveFormat.Location = new System.Drawing.Point(103, 149);
            this.cmbArchiveFormat.Name = "cmbArchiveFormat";
            this.cmbArchiveFormat.Size = new System.Drawing.Size(121, 21);
            this.cmbArchiveFormat.TabIndex = 12;
            // 
            // cmbCompressionLevel
            // 
            this.cmbCompressionLevel.FormattingEnabled = true;
            this.cmbCompressionLevel.Items.AddRange(new object[] {
            "None",
            "Fast",
            "Low",
            "Normal",
            "High",
            "Ultra"});
            this.cmbCompressionLevel.Location = new System.Drawing.Point(352, 149);
            this.cmbCompressionLevel.Name = "cmbCompressionLevel";
            this.cmbCompressionLevel.Size = new System.Drawing.Size(163, 21);
            this.cmbCompressionLevel.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(247, 157);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Nivel de compressão";
            // 
            // chkIncludeEmpty
            // 
            this.chkIncludeEmpty.AutoSize = true;
            this.chkIncludeEmpty.Location = new System.Drawing.Point(2, 185);
            this.chkIncludeEmpty.Name = "chkIncludeEmpty";
            this.chkIncludeEmpty.Size = new System.Drawing.Size(121, 17);
            this.chkIncludeEmpty.TabIndex = 15;
            this.chkIncludeEmpty.Text = "Incluir pastas vazias";
            this.chkIncludeEmpty.UseVisualStyleBackColor = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Multiselect = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(3, 85);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 18;
            this.button2.Text = "Destino";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // rdbFile
            // 
            this.rdbFile.AutoSize = true;
            this.rdbFile.Checked = true;
            this.rdbFile.Location = new System.Drawing.Point(6, 19);
            this.rdbFile.Name = "rdbFile";
            this.rdbFile.Size = new System.Drawing.Size(62, 17);
            this.rdbFile.TabIndex = 19;
            this.rdbFile.TabStop = true;
            this.rdbFile.Text = "Ficheiro";
            this.rdbFile.UseVisualStyleBackColor = true;
            this.rdbFile.Click += new System.EventHandler(this.RadioButton1_CheckedChanged);
            // 
            // rdbFolder
            // 
            this.rdbFolder.AutoSize = true;
            this.rdbFolder.Location = new System.Drawing.Point(105, 19);
            this.rdbFolder.Name = "rdbFolder";
            this.rdbFolder.Size = new System.Drawing.Size(52, 17);
            this.rdbFolder.TabIndex = 20;
            this.rdbFolder.Text = "Pasta";
            this.rdbFolder.UseVisualStyleBackColor = true;
            this.rdbFolder.Click += new System.EventHandler(this.RadioButton2_CheckedChanged);
            // 
            // txtPath
            // 
            this.txtPath.Enabled = false;
            this.txtPath.Location = new System.Drawing.Point(12, 42);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(431, 20);
            this.txtPath.TabIndex = 21;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdbFolder);
            this.groupBox1.Controls.Add(this.rdbFile);
            this.groupBox1.Location = new System.Drawing.Point(12, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(171, 45);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tipo";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(521, 101);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(76, 69);
            this.button1.TabIndex = 23;
            this.button1.Text = "Iniciar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // ZipOperationView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtPath);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.chkIncludeEmpty);
            this.Controls.Add(this.cmbCompressionLevel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbArchiveFormat);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtConfirmPassword);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.groupBox1);
            this.Name = "ZipOperationView";
            this.Size = new System.Drawing.Size(607, 186);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtConfirmPassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbArchiveFormat;
        private System.Windows.Forms.ComboBox cmbCompressionLevel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkIncludeEmpty;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.RadioButton rdbFile;
        private System.Windows.Forms.RadioButton rdbFolder;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
    }
}
