﻿using Projeto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto
{
    public class ZipOperationController
    {
        private readonly IZipOperationView view;
        private IZipOperationModel model;
        private HistoryModel historyModel;

        public ZipOperationController(IZipOperationView view, IZipOperationModel model, HistoryModel historyModel)
        {
            this.view = view;
            this.view.OnSave += View_OnSave;
            this.model = model;
            this.historyModel = historyModel;
        }

        private void View_OnSave(object sender, EventArgs e)
        {
            UpdateModel();
            model.Save();
            historyModel.LogHistory($"{DateTime.Now.ToString("dd-MM-yyyy HH:mm")} - Tentativa de geração efetuada para o ficheiro {model.SavePath};");
        }

        private void UpdateModel()
        {
            if (Enum.TryParse(view.ArchiveFormat, out SevenZip.OutArchiveFormat format))
                model.ArchiveFormat = format;
            else
                model.ArchiveFormat = null;

            if (Enum.TryParse(view.CompressionLevel, out SevenZip.CompressionLevel cmp))
                model.CompressionLevel = cmp;
            else
                model.CompressionLevel = null;

            model.ConfirmPassword = view.ConfirmPassword;
            model.IncludeEmptyDirectories = view.IncludeEmptyDirectories;
            model.Password = view.Password;
            model.SavePath = view.Path;
            model.File = view.File;
            model.Directory = view.Directory;
            model.Type = view.Type;
        }

    }

    

    
}
