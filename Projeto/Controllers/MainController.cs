﻿using Projeto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto.Controllers
{
    public class MainController
    {
        private IMainView view;

        public MainController(IMainView view)
        {
            this.view = view;
            this.view.OnOpenSettings += View_OnOpenSettings;
        }

        private void View_OnOpenSettings(object sender, EventArgs e)
        {
            //Aqui poderiam ser delegadas funções de validação
            //de permissões ao model por forma a verificar se o utilizador
            //pode alterar as definições. Neste caso o mecanismo não é preciso
            view.ShowSettingsView();
        }

    }
}
