﻿using Projeto.Models;
using Projeto.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto.Controllers
{
    public class SettingsController
    {
        private readonly ISettingsView view;
        private readonly SettingsModel model;

        public SettingsController(ISettingsView view, SettingsModel model)
        {
            this.view = view;
            this.view.OnSettingsChanged += View_OnSettingsChanged;
            this.model = model;
        }

        private void View_OnSettingsChanged(object sender, EventArgs e)
        {
            model.LoadSevenZip(view.SevenZipPath);
        }
    }
}
