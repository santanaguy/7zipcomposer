﻿using System;

namespace Projeto.Models
{
    public interface ISettingsModel
    {
        event EventHandler<Result> OnLoadFailed;

        Result LoadSevenZip(string path);
    }
}