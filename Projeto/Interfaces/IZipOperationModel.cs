﻿using System;

namespace Projeto
{
    public interface IZipOperationModel
    {
        string SavePath { get; set; }
        string File { get; set; }
        string Directory { get; set; }
        string Password { get; set; }
        string ConfirmPassword { get; set; }
        SevenZip.OutArchiveFormat? ArchiveFormat { get; set; }
        SevenZip.CompressionLevel? CompressionLevel { get; set; }
        bool IncludeEmptyDirectories { get; set; }
        ItemType Type { get; set; }

        event EventHandler<Exception> OnException;
        event EventHandler<Result> OnSaveOperationFailed;

        void Save();
    }
}