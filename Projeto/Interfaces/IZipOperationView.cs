﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto
{
    public interface IZipOperationView
    {
        bool IncludeEmptyDirectories { get; }
        string CompressionLevel { get; }
        string ArchiveFormat { get; }
        string ConfirmPassword { get; }
        string Password { get; }
        ItemType Type { get; }
        string Directory { get; }
        string File { get; }
        string Path { get; }

        event EventHandler OnSave;
    }
}
