﻿using System;

namespace Projeto.Views
{
    public interface ISettingsView
    {
        string SevenZipPath { get; }

        event EventHandler OnSettingsChanged;
    }
}