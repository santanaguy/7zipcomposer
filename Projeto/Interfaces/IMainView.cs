﻿using Projeto.Models;
using System;

namespace Projeto
{
    public interface IMainView
    {
        event EventHandler OnOpenSettings;

        void ShowSettingsView();
    }
}