﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Projeto.Models
{
    public class HistoryModel
    {
        private readonly string fileName;

        public HistoryModel(string fileName)
        {
            this.fileName = fileName;
        }

        public Result LogHistory(string lines)
        {
            try
            {
                if (!File.Exists(fileName))
                    File.Create(fileName);
                File.AppendAllLines(fileName, new List<string> { lines });
                return Results.OK;
            }
            catch (Exception ex) when (ex is IOException || ex is ArgumentException || ex is SecurityException)
            {
                return Results.LogWasNotSaved;       
            }
        }
    }
}
