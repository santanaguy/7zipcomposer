﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto.Models
{
    public class SettingsModel : ISettingsModel
    {
        public Result LoadSevenZip(string path)
        {
            try
            {
                SevenZip.SevenZipCompressor.SetLibraryPath(path);
                SevenZip.SevenZipCompressor c = new SevenZip.SevenZipCompressor();
                using (var msin = new MemoryStream(new byte[] { 1, 2, 3 }))
                using (var msout = new MemoryStream())
                    c.CompressStream(msin, msout);
                return Results.OK;
            }
            catch (SevenZip.SevenZipLibraryException ex)
            {
                //A razão de utilizar esta exception é para não acoplar as camadas superiores com 
                //types especificos da lib SevenZipSharp.
                throw new LoadException($"Ocorreu um erro a carregar a lib 7zip. " +
                    $"Este erro ocorre geralmente porque não foi possível carregar a biblioteca 7zip. " +
                    $"Verifique se o ficheiro {path} existe." +
                    $"O erro foi: {ex.Message}", ex);
            }
        }

        public event EventHandler<Result> OnLoadFailed;
    }
}
