﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto
{
    public class ZipOperationModel : IZipOperationModel
    {
        public string SavePath { get; set; }
        public string File { get; set; }
        public string Directory { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public SevenZip.OutArchiveFormat? ArchiveFormat { get; set; }
        public SevenZip.CompressionLevel? CompressionLevel { get; set; }
        public bool IncludeEmptyDirectories { get; set; }
        public ItemType Type { get; set; }

        public event EventHandler<Result> OnSaveOperationFailed;
        public event EventHandler<Exception> OnException;

        public void Save()
        {
            bool isEncrypted = !string.IsNullOrEmpty(Password);
            var tmp = SavePath.Split(Path.DirectorySeparatorChar) ?? new string[0];
            tmp[0] = tmp[0]+"\\";
            var directory = Path.Combine(tmp.Take(tmp.Length - 1).ToArray());

            if (string.IsNullOrEmpty(directory) || new DirectoryInfo(directory).Exists == false)
            {
                OnSaveOperationFailed(this, Results.SpecifySavePath);
                return;
            }
            if (Type == ItemType.File)
            {
                if (string.IsNullOrEmpty(File))
                {
                    OnSaveOperationFailed(this, Results.SpecifyFile);
                    return;
                }
                else if (!System.IO.File.Exists(File))
                {
                    OnSaveOperationFailed(this, Results.FileDoesNotExist);
                    return;
                }
            }
            else if (Type == ItemType.Directory)
            {
                if (string.IsNullOrEmpty(Directory))
                {
                    OnSaveOperationFailed(this, Results.SpecifyFolder);
                    return;
                }
                else if (!System.IO.File.Exists(Directory))
                {
                    OnSaveOperationFailed(this, Results.FolderDoesNotExist);
                    return;
                }
            }

            if (!string.IsNullOrEmpty(Password) || !string.IsNullOrEmpty(ConfirmPassword))
                if (Password != ConfirmPassword)
                {
                    OnSaveOperationFailed(this, Results.FolderDoesNotExist);
                    return;
                }

            SevenZip.SevenZipCompressor c = new SevenZip.SevenZipCompressor();
            c.CompressionLevel = CompressionLevel ?? c.CompressionLevel;
            c.ArchiveFormat = ArchiveFormat ?? c.ArchiveFormat;
            c.IncludeEmptyDirectories = IncludeEmptyDirectories;

            try
            {
                if (Type == ItemType.File)
                    if (!isEncrypted)
                        c.CompressFiles(SavePath, File);
                    else
                        c.CompressFilesEncrypted(SavePath, Password, File);
                else if (Type == ItemType.Directory)
                    if (!isEncrypted)
                        c.CompressDirectory(Directory, File);
                    else
                        c.CompressDirectory(Directory, SavePath, Password);
            }
            catch (SevenZip.SevenZipLibraryException ex)
            {
                OnSaveOperationFailed(this, Results.SevenZipNotFound);
            }
        }
    }
}
