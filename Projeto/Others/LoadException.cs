﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Projeto
{
    public class LoadException : ApplicationException
    {
        public LoadException(string message) : base(message)
        {
        }

        public LoadException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected LoadException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
