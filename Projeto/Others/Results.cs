﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto
{
    public class Result
    {
        public Result(bool success, string code, string message)
        {
            Success = success;
            Code = code;
            Message = message;
        }

        public bool Success { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
    }
    public static class Results
    {
        public static Result OK = new Result(true, "", "");
        public static Result EmptyDirectory = new Result(false, "E1", "");
        public static Result SpecifyFile = new Result(false, "E2", "Especifique o ficheiro.");
        public static Result FileDoesNotExist = new Result(false, "E3", "O ficheiro especificado não existe.");
        public static Result SpecifyFolder = new Result(false, "E4", "Especifique a pasta para compressão.");
        public static Result FolderDoesNotExist = new Result(false, "E5", "A pasta especificada não existe.");
        public static Result PasswordNotCorrect = new Result(false, "E6", "Confirme que os campos password e confirmação de password coincidem.");
        public static Result SevenZipNotFound = new Result(false, "E7", "Não foi possível carregar o 7zip. Verifique se o caminho configurado é correto.");
        public static Result SpecifySavePath = new Result(false, "E8", "Indique um caminho de gravação.");
        public static Result LogWasNotSaved = new Result(false, "E9", "Não foi possível gravar o log.");
    }
}
